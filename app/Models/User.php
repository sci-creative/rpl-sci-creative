<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'instagram',
        'package_id',
        'date',
        'time_id',
    ];

    public function time()
    {
        return $this->belongsTo(Time::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function pay()
    {
        return $this->hasMany(Pay::class);
    }
}
