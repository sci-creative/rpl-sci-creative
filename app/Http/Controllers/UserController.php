<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\Time;
use App\Models\User;
use App\Models\Pay;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $user = User::all();
        return view('bayar', ['users' => $user]);
    }

    public function create()
    {
        $time = Time::select('id', 'time')->get();
        $package = Package::select('id', 'service')->get();
        return view('form', compact('time', 'package'));
    }

    public function store(Request $request)
    {
        $user = User::create($request->all());
        return redirect('detail');
    }

    public function show()
    {
        $user = User::with('time', 'package')->latest('id')->first();
        return view('detail', compact('user'));
    }

    public function bukti()
    {
        $user = User::with('time', 'package')->latest('id')->first();
        return view('bayar', compact('user'));
    }

    public function proof(Request $request)
    {
        $pay = Pay::create($request->all());
        $pay = $request->file('bukti');
        return redirect('bayar')->with('status', 'Gambar berhasil diupload!');
    }

    public function pesanan()
    {
        $pay = Pay::all();
        $user = User::with('time', 'package')->latest('id')->first();
        return view('nota', compact('user', 'pay'));
    }
}
