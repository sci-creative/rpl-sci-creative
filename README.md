# Project RPL - SCI Creative
Perkenalkan, kami dari SCI Creative. Pada project mata kuliah Rekaya Perangkat Lunak, kami membuat suatu aplikasi Sewa Studi Foto. 

Kontributor dalam project ini :
- Zaydan Aymar Lutfy
- Ferman Alfiansyah
- Aula Zahra

## Progression
- [ ] [1 - Requirement Analysis](https://gitlab.com/sci-creative/rpl-sci-creative/-/milestones/1)

Semua persyaratan pelanggan dikumpulkan di awal proyek, memungkinkan setiap fase lainnya direncanakan tanpa korespondensi pelanggan lebih lanjut sampai produk selesai. 

- [ ] [2 - Design ](https://gitlab.com/sci-creative/rpl-sci-creative/-/milestones/2)

Proses waterfall sebaiknya dibagi menjadi dua subfase: desain logis dan desain fisik. Subfase desain logis adalah ketika solusi yang mungkin dilakukan brainstorming dan berteori. Subfase desain fisik adalah ketika ide dan skema teoretis tersebut dibuat menjadi spesifikasi konkret.

- [ ] [3 - Implemantion](https://gitlab.com/sci-creative/rpl-sci-creative/-/milestones/3)

Ketika programmer mengasimilasi persyaratan dan spesifikasi dari fase sebelumnya dan menghasilkan kode aktual.

- [ ] [4- Verification](https://gitlab.com/sci-creative/rpl-sci-creative/-/milestones/4)

Ketika client meninjau produk untuk memastikan bahwa produk tersebut memenuhi persyaratan yang ditetapkan di awal proyek waterfall . Ini dilakukan dengan melepaskan produk yang sudah jadi ke client.

- [ ] [5 - Maintenance](https://gitlab.com/sci-creative/rpl-sci-creative/-/milestones/5)

Client secara teratur menggunakan produk selama fase pemeliharaan, menemukan bug, fitur yang tidak memadai, dan kesalahan lain yang terjadi selama produksi. Tim produksi menerapkan perbaikan ini seperlunya sampai client puas.


## Penggunaan
Untuk pengembangan Website kami, berikut teknologi yang dipakai :

- *HTML* 
- *CSS* 
- *JavaScript*
- *PHP*
- *Laravel* 

## Project status
1. User Interface pada aplikasi website sedang dibuat
2. BackEnd dari Website SCI_Creative sedang dibuat.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sci-creative/rpl-sci-creative.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/sci-creative/rpl-sci-creative/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.



