<?php

use App\Http\Controllers\API\DatesController;
use App\Http\Controllers\API\ProfilesController;
use App\Http\Controllers\API\PackagesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('dates', DatesController::class);

Route::apiResource('profiles', ProfilesController::class);

Route::apiResource('packages', PackagesController::class);
