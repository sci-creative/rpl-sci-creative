<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('page');
});

Route::get('/home', [PackageController::class, 'index']);

Route::get('/form', [UserController::class, 'create']);
Route::post('/user', [UserController::class, 'store']);

Route::get('/detail', [UserController::class, 'show']);
Route::get('/bayar', [UserController::class, 'bukti']);
Route::post('/pay', [UserController::class, 'proof']);
Route::get('/nota', [UserController::class, 'pesanan']);



Route::get('/login', function () {
    return view('login');
});
