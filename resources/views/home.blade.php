@extends('layouts.main')

@section('title', 'SCI Creative')

@section('content')
<br><br><br><br>
<h3 class="text-center my-5">Choose an appointment type.</h3>



<div class="container mb-5">
    <div class="card mt-5 shadow">
        <div class="card-body">
            @foreach ($packageList as $item)  
            <a href="http://127.0.0.1:8000/form" type="button" class="col-md-12  btn btn-outline-dark my-3">
                <div class="card-body rounded my-3 bg-light">
                    <h3 class="text-start text-secondary">{{$item->name}}</h3>
                    <div class="card-body m-3 rounded-3 bg-primary">
                        <ul class="text-start list-group">
                            <h5>{{$item->service}}</h5>
                            <li class="list-group">20 Minutes Photoshoot</li>
                            <li class="list-group">10 Minutes Photo Selection</li>
                            <li class="list-group">All Digital Soft File </li>
                        </ul>
                    </div>
                    <ul class="list-inline text-start text-secondary">
                        <li class="list-inline-item">30 minutes</li>
                        <li class="list-inline-item">IDR {{$item->price}}</li>
                    </ul>
                </div>
            </a>
            @endforeach
        </div>

    </div>
</div>
@endsection