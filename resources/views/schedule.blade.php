
@extends('layouts.main')
@section('content')
<br><br><br><br>
<h3 class="text-center my-5">Choose a different time.</h3>

<div class="container bg-primary mt-5 rounded">
    <div class="row justify-content-center mx-0">
        <div class="col-lg-10">
            <div class="card border-0">
                <form autocomplete="off">
                    <div class="card-header bg-light">
                        <div class="mx-0 mb-0 row justify-content-sm-center justify-content-start px-1"> <input type="text" id="datepicker" class="datepicker" placeholder="Pick Date"  name="date" readonly /> </div>
                    </div>
                    <div class="card-body border border-light p-3 p-sm-5">
                        <div class="row text-center mx-0">
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    09:00PM - 09:30PM
                                </a>
                            </div>
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    10:00PM - 10:30PM
                                </a>
                            </div>
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    11:00PM - 11:30PM
                                </a>
                            </div>
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    12:00PM - 12:30PM
                                </a>
                            </div>
                        </div>
                        <div class="row text-center mx-0">
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    13:00PM - 13:30PM
                                </a>
                            </div>
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    14:00PM - 14:30PM
                                </a>
                            </div>
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    15:00PM - 15:30PM
                                </a>
                            </div>
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    16:00PM - 16:30PM
                                </a>
                            </div>
                        </div>
                        <div class="row text-center mx-0">
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    16:00PM - 16:30PM
                                </a>
                            </div>
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    16:00PM - 16:30PM
                                </a>
                            </div>
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    16:00PM - 16:30PM
                                </a>
                            </div>
                            <div class="col-md-3 col-4 my-2">
                                <a href="http://127.0.0.1:8000/form" type="button" class="btn btn-outline-dark">
                                    16:00PM - 16:30PM
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/main.js"></script>
@endsection