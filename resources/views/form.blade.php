@extends('layouts.main')
@section('content')
<br><br><br><br>
<h3 class="text-center mt-5">Confirm Booking</h3>
<p class="text-center ">__________________________________________________________________</p>

<div class="container">
    <div class="bg-secondary card m-5 shadow">
        
        <form class="text-primary m-5" action="user" method="post">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Nama</label>
                <input type="text" class="form-control" id="name" name="name" required >
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email" required>
            </div>
            <div class="mb-3">
                <label for="phone" class="form-label">Nomor HP</label>
                <input type="text" class="form-control" id="phone" name="phone" required>
            </div>
            <div class="mb-3">
                <label for="instagram" class="form-label">Instagram</label>
                <input type="text" class="form-control" id="instagram" name="instagram" required>
            </div>
            <div class="mb-3">
                <label for="service" class="form-label">Service</label>
                <select id="package_id" class="form-select" name="package_id">
                    <option selected>Choose...</option>
                    @foreach ($package as $item)
                    <option value="{{$item->id}}">{{$item->service}}</option>
                    @endforeach

                </select>
            </div>
            <div class="mb-3">
                <label class="form-label" for="date">Pilih Tanggal</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i style="color: #2B2B2B" class="fa-solid fa-calendar"></i></span>
                        </div>
                        {{-- <input type="text" class="form-control" id="autoSizingInputGroup"> --}}
                        <input type="text" id="date" class="form-control" placeholder="Pick Date"  name="date" readonly />
                    </div>
            </div>
            <div class="md-3">
                <label for="time" class="form-label">Pilih Jam</label>
                <select id="time_id" class="form-select" name="time_id">
                    <option selected>Choose...</option>
                    @foreach ($time as $time)
                    <option value="{{$time->id}}">{{$time->time}}</option>
                    @endforeach
                </select>
            </div><br><br>
            <input class="btn bg-light shadow" type="submit" value="Submit">
        </form>
    </div>
</div>
<script src="js/main.js"></script>
@endsection