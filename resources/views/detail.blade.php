@extends('layouts.main')
@section('content')
<br><br><br><br>

    <div class="container mt-5">
        <div class="card m-3 shadow">
            <div class="card-body">
                <h5>Haloo kak {{$user->name}},</h5>
                <p>
                    Pesanan kakak sudah kami terima ya kak, untuk langkah selanjutnya
                    silahkan kakak melakukan pembayaran berikut.
                </p>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row m-3">
            <div class="col-md card me-3 shadow">
                <div class="card-body">
                    <h5>Detail pesanan</h5>
                    <h6>____________________</h6><br>
                    <h5>Nama  : {{$user->name}}</h5>
                    <h5>No HP : {{$user->phone}}</h5>
                    <p>Tanggal : {{$user->date}}</p>
                    <p>Jam : {{$user->time->time}}</p>
                </div>
            </div>
            <div class="col-md card ms-3 shadow">
                <div class="card-body">
                    <h6>Price</h6>
                    <p>IDR {{$user->package->price}}</p>
                    <h6>Service</h6>
                    <p>{{$user->package->name}}</p>
                    <h6>Service description</h6>
                    <p>{{$user->package->service}}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="card-body text-end m-3">
            <a href="http://127.0.0.1:8000/bayar" type="button" class="btn btn-light text-primary shadow">Bayar</a>
        </div>
    </div>

    @endsection