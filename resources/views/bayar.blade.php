@extends('layouts.main')
@section('content')
<br><br><br><br>

    <div class="container mt-5">
        <div class="card m-3 shadow">
            <div class="card-body">
                <h5>Haloo kak {{$user->name}},</h5>
                <p>
                    Silahkan kakak melakukan pembayaran sebesar IDR {{$user->package->price}}
                </p>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row m-3">
            <div class="col-md card me-3 shadow">
                <div class="card-body">
                    <h5>No Rekening </h5>
                    <h6>__________________</h6><br>
                    <h5>XXX XXX XXX XXX</h5>
                    <p>a/n ZAYDAN AYYMAR LUTHFY</p>
                </div>
            </div>
            <div class="col-md card ms-3 shadow">
                <div class="card-body">
                    <h5>Bukti Pembayaran</h5>
                    <h6>__________________________</h6><br>
                    <div class="card-body ">
                        <button type="button" class="btn btn-light text-primary shadow" data-bs-toggle="modal" data-bs-target="#modalbukti" ><i class="fa fa-plus"></i> Upload Bukti</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
<div class="modal fade" id="modalbukti" tabindex="-1" aria-labelledby="modalbukti" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="pay" method="post">
                    @csrf
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Bukti Pembayaran</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="bukti" class="form-label">Upload Disini</label>
                        <input class="form-control" type="file" id="bukti" name="bukti" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-light text-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

    <div class="container mt-5">
        <div class="card m-3 shadow">
            <div class="card-body">
                <p class="">
                    Kalau kakak ingin mengganti jadwal atau membatalkan pesanan, kami memberikan
                    maksimal 1 hari sebelum sesi foto dan hanya dapat melakukan reschedule sebanyak 1 kali.
                    Jika tidak ada konfirmasi DP tidak akan dikembalikan ya kak.
                </p>
                <br><br><br>
                <p>Trimakasih</p>
                <p>Salam,</p>
                <p>SCI CREATIVE</p>
                <h6>________________________________________________________________________________________________________________________________________________________________________________________</h6>
            </div>
            <div class="card-body text-end mx-3">
                <a href="http://127.0.0.1:8000/nota" type="button" class="btn btn-light text-primary shadow">Kirim</a>
            </div>
        </div>
    </div>

@endsection