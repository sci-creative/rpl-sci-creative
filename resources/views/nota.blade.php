<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <title>SCI Creative</title>
</head>

<body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top shadow p-3 mb-5 bg-body rounded" id="mainNav">
        <div class="container">
            <a class="navbar-brand ms-5 fw-bold fs-2" href="#">
                <img class="me-3" src="img/logo.png" alt="" width="100">
                SCI Creative
            </a><br>
        </div>
    </nav>
    <br><br><br><br>
    <div class="container mt-5">
        <h4 class="m-3">Nota Pemesanan</h4>
        <div class="row m-3">
            <div class="col-md-5 card me-3 shadow">
                <div class="card-body">
                    <h6>Nama : {{$user->name}}</h6>
                    <h6>Email : {{$user->email}}</h6>
                    <h6>No HP : {{$user->phone}}</h6>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row m-3">
            <div class="col-md-2  me-3 ">
                <ul class="list-group text-start">
                    <h5>No Pesanan</h5>
                    <li class="list-group">ASP0{{$user->id}}</li>
                </ul>
            </div>
            <div class="col-md-2  me-3 ">
                <ul class="list-group text-start">
                    <h5>Waktu Pembayaran</h5>
                    <li class="list-group">{{$user->created_at}}</li>
                </ul>
            </div>
            <div class="col-md-2  me-3 ">
                <ul class="list-group text-start">
                    <h5>Pembayaran</h5>
                    <li class="list-group">Lunas</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <h4 class="m-3">Rincian Pemesanan</h4>
        <div class="row m-3">
            <div class="col-md-5 card me-3 shadow">
                <div class="card-body">
                    <h6>Tanggal : {{$user->date}}</h6>
                    <p>Jam : {{$user->time->time}}</p>
                    <h6>Nama Paket</h6>
                    <p>{{$user->package->name}}</p>
                    <h6>Price</h6>
                    <p>IDR {{$user->package->price}}</p>
                </div>
            </div>
            <div class="col-md-5 card me-3 shadow">
                <div class="card-body">
                    <h6>Service description</h6>
                    <p>{{$user->package->service}}</p>
                    <div class="card-body bg-light rounded">
                        <ul class="list-group text-start">
                            <li class="list-group">20 Minutes Photoshoot</li>
                            <li class="list-group">10 Minutes Photo Selection</li>
                            <li class="list-group">All digital Soft files</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container my-5">
        <div class="col-md-10 card-body me-3 text-end">
            <a href="http://127.0.0.1:8000/" type="button" class="btn btn-light text-primary shadow">Selesai</a>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>